import { Component, OnInit, ViewChild } from '@angular/core'; 
import { DbService } from '../../services/db/db.service';
import { TicketComponent } from '../../components/ticket/ticket.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';  

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit { 
  
  tickets = [];
 
  data_view = 'treeGrid';

  constructor(
    private _db: DbService, 
    public _dialog: MatDialog,
    public _snackBar: MatSnackBar
    ) {  }

 

  ngOnInit() {
    this.tickets = this._db.getAll();
    
  }

  openDialog(data): void {
    const dialogRef = this._dialog.open( TicketComponent, { 
      width: '600px',
      data: data
    }); 
    

  }

  deleteTicket(airline_id, ticket_id){
 
    if(  this.tickets[ airline_id ].tickets.splice( ticket_id, 1 ) ){
      this._snackBar.open('Passagem Excluida!', 'Fechar', { duration: 3000, verticalPosition: 'top' });
    } else {
      this._snackBar.open('Não foi possivel excluir a Passasgem!', 'Fechar', { duration: 3000, verticalPosition: 'top' });
    }
  }
      
}

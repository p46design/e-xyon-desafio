import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: ['l', 'LL'],
  },
  display: {
    dateInput: 'DD/MM/YYYY'
  },
};

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  providers: [
     {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
 
})

export class TicketComponent implements OnInit {
  ticket_bkp:any;
  ticket = {
      flight_number: '',
      seat: '',
      departure_date: new FormControl(),
      departure_hour: '',
      cpf: '',
      name:  '',
      surname: '',
      ticket_value: 0
    };
  seats = [];

  constructor(public dialogRef: MatDialogRef<TicketComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  saveTicket(): void {
    console.log('Resultado da edição dos campos.', this.ticket);

    for (let [key, val] of Object.entries(this.ticket) ) {
      this.ticket_bkp[key] = key === 'departure_date' ? this.ticket.departure_date.value : val ;
    }
    this.dialogRef.close();
  }

  ngOnInit() {

    this.ticket_bkp = this.data;

    for (let [key, value] of Object.entries(this.ticket_bkp) ) {
      this.ticket[key] = value;
    }
    
    this.ticket.departure_date = new FormControl(moment(this.ticket_bkp.departure_date));

    for(let i = 1; i <= 48; i++ ){
      let type = ['A', 'B', 'C'];
      type.map( row => {
        this.seats.push( i + row );
      } );
    }

  }

}

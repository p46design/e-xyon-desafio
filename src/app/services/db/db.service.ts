import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  public tickets_query = [
    {
      airline : 'GOL',
      tickets: [
        {
          flight_number : '000001',
          seat: '44B', 
          departure_date: '2021-08-22',
          departure_hour: '13:14', 
          cpf: '123.456.789-10',
          name: 'Paulo',
          surname: 'Nascimento',
          ticket_value: 1180.57,
          tree_state: false,
          tree_select: false
        },
        {
          flight_number : '000002',
          seat: '33C',
          departure_date: '2021-05-29', 
          departure_hour: '22:05', 
          cpf: '123.456.789-10',
          name: 'Júlio',
          surname: 'Fazenda',
          ticket_value: 180.57,
          tree_state: false,
          tree_select: false
        },
  
      ],
      tree_state: false,
      tree_select: false
    },
    {
      airline : 'Avianca',
      tickets: [
        {
          flight_number : '000003',
          seat: '11A',
          departure_date: '2021-10-22', 
          departure_hour: '15:30', 
          cpf: '123.456.789-10',
          name: 'Mariana',
          surname: 'Moraes',
          ticket_value: 1280.57,
          tree_state: false,
          tree_select: false
        } 
  
      ],
      tree_state: false,
      tree_select: false
    },
    {
      airline : 'Azul',
      tickets: [],
      tree_state: false,
      tree_select: false
    }
  ];

  constructor() { }

  getAll(){
    return this.tickets_query;
  }

  getItem(arline_id, ticket_id){
    return this.tickets_query[arline_id].tickets[ticket_id];
  }
}


